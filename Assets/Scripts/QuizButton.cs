﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using Quiz;

public class QuizButton : MonoBehaviour
{
    [SerializeField]
    Text label;
    [SerializeField]
    Button button;
    int id;

    Question question;
    List<Question> questions;

    GameObject stateUI;

    public Action<int, Question, List<Question>, GameObject> onClick;
    public void Init(int id, string text, Question question, List<Question> questions,  GameObject stateUI)
    {
        this.id = id;
        label.text = text;
        this.question = question;
        this.questions = questions;
        this.stateUI=stateUI;
    }

    private void Start()
    {
        button.onClick.AddListener(OnButtonPressed);
    }
    public void OnButtonPressed()
    {
        if (onClick != null)
        {
            onClick(id, question, questions, stateUI);
        }
    }

    private void OnDestroy()
    {
        button.onClick.RemoveAllListeners();
    }
}
