﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using Quiz;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class QuizUI : MonoBehaviour
{
    [SerializeField]
    Button langButton, startButton;

    [SerializeField]
    Controller controller;

    [SerializeField]
    GameObject stateUI, gameUI, mainmenuUI;
    private List<Question> questionsList;
    [SerializeField]
    Button restart;
    Question currentQuestion;

    QuizButton curButton;
    private void Awake()
    {


        var filePath = Path.Combine(Application.streamingAssetsPath + "/", "Questions.json");
        StreamReader reader = new StreamReader(filePath);
        string dataAsJson = reader.ReadToEnd();
        reader.Dispose();
        questionsList = Quiz.QuestionsList.FromJson(dataAsJson).Questions;
    }
    void Start()
    {
        stateUI.gameObject.SetActive(false);
        gameUI.gameObject.SetActive(false);
        langButton.onClick.AddListener(() => { ButtonClickLang(langButton, GameStat.curLang); });
        restart.onClick.AddListener(ButtonClickRestart);
        startButton.onClick.AddListener(ButtonStart);
    }


    public void ButtonClickLang(Button button, string lang)
    {
        if (lang == "Ru")
        {
            button.GetComponentInChildren<Text>().text = "Kz";
            GameStat.curLang = "Kz";
        }
        else if (lang == "Kz")
        {
            button.GetComponentInChildren<Text>().text = "En";
            GameStat.curLang = "En";
        }
        else if (lang == "En")
        {
            button.GetComponentInChildren<Text>().text = "Ru";
            GameStat.curLang = "Ru";
        }
    }
    public void ButtonStart()
    {
        mainmenuUI.gameObject.SetActive(false);
        gameUI.gameObject.SetActive(true);
        controller.LoadButtons(questionsList, stateUI);
    }
    

    void ButtonClickRestart()
    {
        GameStat.isRightAnswers = 0;
        GameStat.isFalseAnswers = 0;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    private void OnDestroy()
    {
        restart.onClick.RemoveAllListeners();
        langButton.onClick.RemoveAllListeners();
        startButton.onClick.RemoveAllListeners();
    }
}
