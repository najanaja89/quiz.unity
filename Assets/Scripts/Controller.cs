﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using Newtonsoft.Json;
using Quiz;

public class Controller : MonoBehaviour
{
    [SerializeField]
    QuizButton quizButton;
    [SerializeField]
    Transform buttonParent;

    private void Start()
    {

    }
    public void LoadButtons(List<Question> questions, GameObject stateUI)
    {

        var question = LoadCurrentQuestion(questions);

        if (question == null)
        {
            stateUI.gameObject.SetActive(true);
            var rightAnswers = GameObject.Find("StatTextRight").GetComponent<Text>();
            rightAnswers.text = "Right Answers: " + GameStat.isRightAnswers.ToString();
            var wrongAnswers = GameObject.Find("StatTextFalse").GetComponent<Text>();
            wrongAnswers.text = "Wrong Answers: " + GameStat.isFalseAnswers.ToString();
        }
        else
        {
            for (int i = 0; i < question.Options.Count; i++)
            {
                var button = Instantiate(quizButton, buttonParent);
                if (GameStat.curLang == "Ru")
                {
                    button.Init(i, question.Options[i].NameRu, question, questions, stateUI);
                }
                else if (GameStat.curLang == "Kz")
                {
                    button.Init(i, question.Options[i].NameKz, question, questions, stateUI);
                }
                else
                {
                    button.Init(i, question.Options[i].NameEng, question, questions, stateUI);
                }
                button.onClick += ButtonPress;
            }
            questions.Remove(question);
        }
    }

    void ButtonPress(int id, Question question, List<Question> questions, GameObject stateUI)
    {
        Debug.Log("Button pressed id" + id + " " + question.RightAnswerId);
        if (question.RightAnswerId == id)
        {
            RemoveAllButtons();
            GameStat.isRightAnswers++;
        }
        else
        {
            RemoveAllButtons(); 
            GameStat.isFalseAnswers++;
        };
         LoadButtons(questions, stateUI);
    }

    private void RemoveAllButtons()
    {
        GameObject[] allObjects = GameObject.FindGameObjectsWithTag("ButtonClone");
        foreach (GameObject obj in allObjects)
        {
            Destroy(obj);
        }

    }

    public Question LoadCurrentQuestion(List<Question> questions)
    {
        for (var i = 0; i < questions.Count; ++i)
        {
            if (questions[i].isChecked == false)
            {
                var questionUI = GameObject.Find("QuestionTextUI").GetComponent<Text>();
                if (GameStat.curLang == "Ru") questionUI.text = questions[i].NameRu;
                else if (GameStat.curLang == "Kz") questionUI.text = questions[i].NameKz;
                else questionUI.text = questions[i].NameEng;
                return questions[i];
            }

        }
        return null;
    }
}
