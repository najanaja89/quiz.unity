﻿
using System;
using System.Collections.Generic;
using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;


namespace Quiz
{
    public partial class QuestionsList
    {
        [JsonProperty("Questions")]
        public List<Question> Questions { get; set; }
    }

    public partial class Question
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("name_ru")]
        public string NameRu { get; set; }

        [JsonProperty("name_eng")]
        public string NameEng { get; set; }

        [JsonProperty("name_kz")]
        public string NameKz { get; set; }

        [JsonProperty("right_answer_id")]
        public long RightAnswerId { get; set; }

        [JsonProperty("options")]
        public List<Option> Options { get; set; }

        public bool isChecked { get; set; } = false;
    }

    public partial class Option
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("name_ru")]
        public string NameRu { get; set; }

        [JsonProperty("name_eng")]
        public string NameEng { get; set; }

        [JsonProperty("name_kz")]
        public string NameKz { get; set; }
    }

    public partial class QuestionsList
    {
        public static QuestionsList FromJson(string json) => JsonConvert.DeserializeObject<QuestionsList>(json, Quiz.Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this QuestionsList self) => JsonConvert.SerializeObject(self, Quiz.Converter.Settings);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}
